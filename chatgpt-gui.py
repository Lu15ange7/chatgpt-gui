#!/usr/bin/env python
import sys, os
import configparser
import openai
from ui_window import *
from PySide2.QtWidgets import *
from PySide2.QtCore import *

#LOAD API FROM "API.INI" FILE
config = configparser.ConfigParser()
config.read('api.ini')
setting = config.get('Default', 'API')
openai.api_key = setting

#CODE FROM THE FORM
class Widget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ui.pushButton.clicked.connect(self.send_text)

    def send_text(self):

        #OPENIA ENGINE
        prompt = self.ui.lineEdit.text()
        completions = openai.Completion.create(
            engine="text-davinci-002",
            prompt=prompt,
            max_tokens=1024,
            n=1,
            stop=None,
            temperature=0.5,
        )

        message = completions.choices[0].text

        self.ui.textEdit.setText(message)

def main():
    app = QApplication(sys.argv)
    widget = Widget()
    widget.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    main()
