# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'windowwBIAyA.ui'
##
## Created by: Qt User Interface Compiler version 5.15.8
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *  # type: ignore
from PySide2.QtGui import *  # type: ignore
from PySide2.QtWidgets import *  # type: ignore

class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(600, 600)
        Form.setMinimumSize(QSize(600, 600))
        Form.setMaximumSize(QSize(600, 600))
        icon = QIcon()
        icon.addFile(u"chatgpt-icon.svg", QSize(), QIcon.Normal, QIcon.Off)
        Form.setWindowIcon(icon)
        Form.setStyleSheet(u"background-color: rgb(52, 54, 65);\n"
"color: rgb(255, 255, 255);\n"
"border-radius:10px;")
        self.textEdit = QTextEdit(Form)
        self.textEdit.setObjectName(u"textEdit")
        self.textEdit.setEnabled(False)
        self.textEdit.setGeometry(QRect(30, 150, 541, 361))
        self.textEdit.setStyleSheet(u"background-color: rgb(68, 70, 84);")
        self.lineEdit = QLineEdit(Form)
        self.lineEdit.setObjectName(u"lineEdit")
        self.lineEdit.setGeometry(QRect(30, 539, 451, 41))
        self.lineEdit.setStyleSheet(u"background-color: rgb(68, 70, 84);")
        self.lineEdit.setAlignment(Qt.AlignCenter)
        self.pushButton = QPushButton(Form)
        self.pushButton.setObjectName(u"pushButton")
        self.pushButton.setGeometry(QRect(490, 539, 84, 41))
        self.pushButton.setStyleSheet(u"background-color: rgb(16, 163, 127);")
        icon1 = QIcon()
        iconThemeName = u"key-enter"
        if QIcon.hasThemeIcon(iconThemeName):
            icon1 = QIcon.fromTheme(iconThemeName)
        else:
            icon1.addFile(u"../../../.designer/backup", QSize(), QIcon.Normal, QIcon.Off)
        
        self.pushButton.setIcon(icon1)
        self.pushButton.setIconSize(QSize(30, 30))
        self.label = QLabel(Form)
        self.label.setObjectName(u"label")
        self.label.setGeometry(QRect(30, 30, 91, 91))
        self.label.setPixmap(QPixmap(u"chatgpt-icon.svg"))
        self.label.setScaledContents(True)
        self.label_2 = QLabel(Form)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setGeometry(QRect(140, 30, 421, 91))
        font = QFont()
        font.setPointSize(20)
        self.label_2.setFont(font)
        QWidget.setTabOrder(self.lineEdit, self.pushButton)
        QWidget.setTabOrder(self.pushButton, self.textEdit)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"ChatGPT-GUI", None))
        self.lineEdit.setText("")
        self.pushButton.setText("")
        self.label.setText("")
        self.label_2.setText(QCoreApplication.translate("Form", u"<html><head/><body><p align=\"center\"><span style=\" font-size:20pt;\">CHATGPT-GUI</span></p><p align=\"center\"><span style=\" font-size:9pt;\">A ChatGPT Client made for desktop written in Python</span></p></body></html>", None))
    # retranslateUi

