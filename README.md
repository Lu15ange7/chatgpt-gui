# ChatGPT-GUI

A ChatGPT Client made for desktop written in Python

![ChatGPT-GUI](https://gitlab.com/Lu15ange7/chatgpt-gui/-/raw/main/chatgpt-gui-preview.png "ChatGPT-GUI")

## Dependencies and Requirements

Before of use, please install these dependencies.

```
pip install configparser
pip install openai
pip install PySide2
```

Just follow this commands for download and enter the folder.

```
git clone https://gitlab.com/Lu15ange7/chatgpt-gui.git
cd chatgpt-gui
```
Add you API KEY in `api.ini`.
```
[Default]
API = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

To get an API key, you need to register on this page
https://platform.openai.com

## Execute

Now execute the software.

```
python chatgpt-gui.py
```
Enjoy it.
